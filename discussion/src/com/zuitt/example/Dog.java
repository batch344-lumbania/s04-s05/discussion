package com.zuitt.example;

public class Dog extends Animal{

    public Dog() {
        super();
    }

    public Dog(String name, String color) {
        super(name, color);
    }
}
