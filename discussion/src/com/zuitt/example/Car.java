package com.zuitt.example;

public class Car {
    //    Access Modifiers
    //    These are used to restrict the scope of a class, constructor, variable, method, or data member
    //    Four Types of Access Modifiers:
    //      1. Default - No keyword indicated (Accessibility is within the package.)
    //      2. Private (private) - Properties or method is only accessible within the class.
    //      3. Protected (protected) - properties and methods are only accessible by the class of the same package and the subclass present in any package.
    //      4. Public (public) - properties and methods that can be accessed from anywhere.

    //    Class Creation
    //        1. Properties - characteristic of an object
    private String name;
    private String brand;
    private int yearOfMake;

    private Driver driver;

    //        2. Constructor - used to create/instantiate an object.
    //            a. Empty Constructor - creates object that doesn't have any arguments or parameters. It is also referred as default constructor.
    public Car() {

    }

    //            b. Parameterized constructor - creates an object with argument/parameters
    public Car(String name, String brand, int yearOfMake) {
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver("Manong");
    }
    //        3. Getters and Setters - get and set the values of each property of an object. Keep in mind that the number of setters and getters depends on the number of properties.
    //            Getters - retrieves the value of instantiate object.

    //            getter for name property:
    public String getName() {
        return this.name;
    }

    //            getter for brand property:
    public String getBrand() {
        return this.brand;
    }
//            getter for year of make
    public int getYearOfMake() {
        return this.yearOfMake;
    }

    public String getDriver() {
        return this.driver.getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake) {
        this.yearOfMake = yearOfMake;
    }

    public void setDriver(String name) {
        this.driver.setName(name);
    }
    //        4. Methods - functions an object can perform. (Behavior)

    public void drive() {
        System.out.println("The car is running. Vroom vroom");
    }

}
