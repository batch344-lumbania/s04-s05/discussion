package com.zuitt.example;

public class Person implements Actions, Greetings{
    public void sleep() {
        System.out.println("zzzzzzz");
    }

    public void run() {
        System.out.println("Runinnn . . . . . .");
        System.out.println("Runinnn . . . . . .");
    }

    public void morningGreet() {
        System.out.println("Goodmorning!");
    }

    public void afternoonGreet() {
        System.out.println("Good afternoon!");
    }
}
