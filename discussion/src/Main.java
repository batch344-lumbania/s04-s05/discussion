import com.zuitt.example.Car;
import com.zuitt.example.Dog;
import com.zuitt.example.Person;

public class Main {
    public static void main(String[] args) {
        //        OOP
        //        OOP stands for "Object Oriented Programming"
        //        OOP is programming model that allows developers to design software around data or objects, rather than function and logic.

        //        OOP Concepts
        //        Object - abstract idea that represents something in the real world.
        //            Example: The concept of a dog.
        //        Class - representation of object using code. Blueprint/Framework
        //            Example: Writing code that would describe a dog.
        //        Instance - Unique copy of the idea, made physical
        //            Example: Instantiating a dog named Brownie from the dog class.

        //        Objects
        //        States and attributes - what is the idea about? It would describe the properties that the object has.
        //        Behaviors - what can the idea do?
        //            Example: A person has attributes like name, age, height, weight etc. A person can also eat, sleep, and speak.

        //        Four Pillars of OOP
        //        1. Encapsulation
        //            A mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit.
        //            "Data Handling" - the variables of a class will be hidden from other classes, and can only be accessed through the methods of the current class.
        //            Variables/properties are in private
        //            Requires a public getter and setter methods.

        //Instantiate a class Car with empty argument.
        Car myCar = new Car("Vios", "Toyota", 2025);

        //getters
        System.out.println(myCar.getName());
        System.out.println(myCar.getBrand());
        System.out.println(myCar.getYearOfMake());

        //setters
        myCar.setName("Conan");
        System.out.println(myCar.getName());

        Car emptyCar = new Car();
        System.out.println(emptyCar.getName());

        System.out.println();

        //        Composition and Inheritance
        //            Both of these concepts promotes code reuse through different approach
        //        Inheritance - allows modelling an object that is a subset of another object.
        //        Composition allows modelling object that are made up of other objects

        Dog cutie = new Dog("Milo", "Brown");
        System.out.println(cutie.getName());
        cutie.call();

        //        Abstraction
        //        is a process where all the logic and complexity are hidden from the user

        //        Interface
        //        Is used to achieve total abstraction
        //        Creating Abstract classes doesn't support multiple inheritance
        Person child = new Person();
        child.sleep();

//        Polymorphism
//            Greek word poly (many) and morph (forms)
//            In short, many forms

//            Two main types:
//            Static or compile time polymorphism
//            Dynamic or run time polymorphism
    }
}